import pygame
import random

# Initialize Pygame
pygame.init()

# Colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)

# Screen dimensions
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# Grid dimensions
LARGE_GRID_ROWS = 9  # Limit to 9 rows
LARGE_GRID_COLS = 9  # Limit to 9 columns
SMALL_GRID_ROWS = 3
SMALL_GRID_COLS = 3

# Create the screen
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Mosaic Browsing Game")

# Generate random grid
def generate_random_grid(rows, cols):
    return [[random.choice([0, 1]) for _ in range(cols)] for _ in range(rows)]

# Draw a grid
def draw_grid(grid, x_offset, y_offset):
    cell_size = 40
    for row in range(len(grid)):
        for col in range(len(grid[0])):
            color = WHITE if grid[row][col] == 1 else BLACK
            pygame.draw.rect(screen, color, (col * cell_size + x_offset, row * cell_size + y_offset, cell_size, cell_size))

def main():
    clock = pygame.time.Clock()
    running = True
    won = False

    # Generate random grids
    large_grid = generate_random_grid(LARGE_GRID_ROWS, LARGE_GRID_COLS)
    solution_grid = generate_random_grid(LARGE_GRID_ROWS - SMALL_GRID_ROWS + 1, LARGE_GRID_COLS - SMALL_GRID_COLS + 1)

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN and not won:
                mouse_x, mouse_y = pygame.mouse.get_pos()
                small_grid_x = (mouse_x - 100) // 40
                small_grid_y = (mouse_y - 100) // 40

                if 0 <= small_grid_x < LARGE_GRID_COLS - SMALL_GRID_COLS + 1 and 0 <= small_grid_y < LARGE_GRID_ROWS - SMALL_GRID_ROWS + 1:
                    match = True
                    for row in range(SMALL_GRID_ROWS):
                        for col in range(SMALL_GRID_COLS):
                            if large_grid[small_grid_y + row][small_grid_x + col] != solution_grid[row][col]:
                                match = False
                                break
                        if not match:
                            break

                    if match:
                        won = True

        screen.fill(BLACK)

        # Draw the larger grid
        draw_grid(large_grid, 100, 100)

        # Draw the solution grid as an overlay (translucent)
        if not won:
            for row in range(len(solution_grid)):
                for col in range(len(solution_grid[0])):
                    if solution_grid[row][col] == 1:
                        pygame.draw.rect(screen, (0, 0, 255, 100), (col * 40 + 100, row * 40 + 100, 40, 40))

        # Draw the smaller grid at mouse position
        mouse_x, mouse_y = pygame.mouse.get_pos()
        small_grid_x = (mouse_x - 100) // 40
        small_grid_y = (mouse_y - 100) // 40
        small_grid = [[0] * SMALL_GRID_COLS for _ in range(SMALL_GRID_ROWS)]

        if 0 <= small_grid_x < LARGE_GRID_COLS - SMALL_GRID_COLS + 1 and 0 <= small_grid_y < LARGE_GRID_ROWS - SMALL_GRID_ROWS + 1:
            for row in range(SMALL_GRID_ROWS):
                for col in range(SMALL_GRID_COLS):
                    small_grid[row][col] = large_grid[small_grid_y + row][small_grid_x + col]

            draw_grid(small_grid, 100, 100)

        if won:
            pygame.draw.rect(screen, GREEN, (100 + small_grid_x * 40, 100 + small_grid_y * 40, SMALL_GRID_COLS * 40, SMALL_GRID_ROWS * 40), 5)

            # Draw a message indicating victory
            font = pygame.font.Font(None, 36)
            text = font.render("Congratulations! You found the pattern!", True, WHITE)
            text_rect = text.get_rect(center=(SCREEN_WIDTH // 2, SCREEN_HEIGHT - 50))
            screen.blit(text, text_rect)

        pygame.display.flip()
        clock.tick(60)

    pygame.quit()

if __name__ == "__main__":
    main()
